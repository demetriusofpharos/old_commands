#!/bin/bash

declare -a podname
itr=1
getpods=$($(command -v kubectl) get pods -o=jsonpath='{range .items[*]}{"\n"}{.metadata.name}{end}' | sort)

for line in $getpods
 do
  $(command -v echo) "#$itr: $line"
  podname[$itr]="$line"
  itr=$itr+1
done

if [[ ${#podname[@]} == 1 ]]; then
  echo "Using only pod available..."
  podtorun=1
  sleep 1
else
  read -p "Select pod #: " podtorun
fi

$(command -v kubectl) exec ${podname[${podtorun}]} -- $1 $2 $3 $4 $5 $6 $7
