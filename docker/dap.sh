#!/usr/bin/env bash

build_docker()
{
  $(command -v docker-compose) -f $REPO_DIR/docker-$1.yml build $2
}

build_ssl()
{
  $(command -v sudo) $(command -v openssl) req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout $REPO_DIR/docker/nginx/certs/ocr-dap.key -out $REPO_DIR/docker/nginx/certs/ocr-dap.crt
}

composer_update()
{
  prefix="ocrdap_"
  database=""
  if [[ "$1" == "stage" ]]; then
    prefix="ocrdap_stage_"
  fi

  $(command -v printf) "\n\nUpdating DAP composer...\n"
  #$(command -v rm) -rf $REPO_DIR/submodules/ocr-dataacquisitionportal/app/library/vendor
  $(command -v docker) exec -it -w /var/www/dap/app/library "${prefix}api-fpm" composer update

  $(command -v printf) "\n\nUpdating Proxy composer...\n"
  #$(command -v rm) -rf $REPO_DIR/submodules/ocr-proxyprovider/app/library/vendor
  $(command -v docker) exec -it -w /var/www/proxyprovider/app/library "${prefix}proxy-fpm" composer update
}

import_portal()
{
  $(command -v cat) $REPO_DIR/submodules/ocr-dataacquisitionportal/app/config/db/ocr-dataacquisitionportal.sql | $(command -v docker) exec -i ocrdap_db /usr/bin/mysql -u root --password=U50hzPgdrqpY2Srbw ocr-dataacquisitionportal
}

start_docker()
{
  $(command -v docker-compose) -f $REPO_DIR/docker-$1.yml up $2
}

stop_docker()
{
  $(command -v docker-compose) -f $REPO_DIR/docker-$1.yml down
}

update_portal()
{
  container="ocrdap_api-fpm"
  if [[ "$1" == "stage" ]]; then
    container="ocrdap_stage_api-fpm"
  fi
  $(command -v docker) exec -it -w /var/www/dap $container php app/cli.php populate all reset
}

view_logs() 
{
  $(command -v docker-compose) -f docker-$1.yml logs --tail=0 --follow
}

OLD_DIR="$(pwd)"
ARGS=":c:d:e:l:q:s:"
LOG="-d"

while getopts $ARGS option; do
  case "${option}"
    in
      c) CACHE=$([[ ${OPTARG}=="false" ]] && echo "--no-cache" || echo "")
      ;;
      d) DATA=${OPTARG}
      ;;
      e) ENV=${OPTARG}
      ;;
      l) LOG=$([[ ${OPTARG}=="show" ]] && echo ${OPTARG} || echo "")
      ;;
      q) QUIT=${OPTARG}
      ;;
      s) SSL=$([[ ${OPTARG}=="true" ]] && echo ${OPTARG} || echo "")
      ;;
      \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done


#SET WORKING DIRECTORY
if [[ "$ENV" != "dev" ]]; then
  REPO_DIR="/var/docker/dap/ocr-dap-docker"
else
  REPO_DIR=$OLD_DIR
fi


#SHOW LOGS
if [[ "$LOG" == "show" ]]; then
  $(command -v printf) "\n\nViewing logs...\n"
  view_logs $ENV
  exit 1
fi


#QUIT CONTAINERS
if [[ "$QUIT" == "true" ]]; then
  $(command -v printf) "\n\nStopping docker containers and quitting...\n"
  stop_docker $ENV
  exit 1
fi


#GENERATE SSL
if [[ "$SSL" == "true" ]]; then
  $(command -v printf) "\n\nCreating SSL certificates...\n"
  build_ssl
fi


#STOP DOCKER
$(command -v printf) "\n\nStopping docker...\n"
stop_docker $ENV


#UPDATE CODE
if [[ "$ENV" != "dev" ]]; then
  $(command -v printf) "\n\nUpdating submodules...\n"
  $(command -v cd) $REPO_DIR
  $(command -v git) submodule init
  $(command -v git) submodule update
fi


#BUILD DOCKER CONTAINERS
$(command -v printf) "\n\nBuilding docker from cache...\n"
$(command -v cd) $OLD_DIR
build_docker $ENV $CACHE


#START DOCKER CONTAINERS
$(command -v printf) "\n\nStarting docker...\n"
start_docker $ENV $LOG


#UPDATE DATA
if [[ "$DATA" != "" ]] && [[ "$ENV" != "prod" ]]; then
  $(command -v printf) "\n\nHitting snooze alarm (waiting for container to spin up)...\n"
  sleep 20

  if [[ "$DATA" == "both" ]]; then
    $(command -v printf) "\n\nImporting database 'da_portal' ...\n"
    import_portal 
  fi

  $(command -v printf) "\n\nUpdating database 'da_portal' ...\n"
  update_portal $ENV 
fi


#UPDATE composer
if [[ "$ENV" != "" ]]; then
  $(command -v printf) "\n\nRunning composer...\n"
  $(command -v cd) $REPO_DIR
  composer_update $ENV
fi
