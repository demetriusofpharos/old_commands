#!/bin/bash

APP=0
INT=1
NUM=224
ext[1]=".pdf"
ext[2]=".mobi"
ext[3]=".epub"

URLBASE="https://ia601502.us.archive.org/21/items/starlog_magazine-"
TITLEBASE="starlog_issue_"
DIRBASE="starlog/"

echo ;

while [ $INT -le $NUM ];
  do
	DIR=$INT
	if [ $DIR -lt 100 ]
	  then
	  DIR=$APP$DIR
    fi
	if [ $DIR -lt 10 ]
      then
	  DIR=$APP$DIR
    fi
	echo "mkdir $DIRBASE$DIR..."
	echo ;
	mkdir $DIRBASE$DIR
	i=1
    while [ $i -le 3 ];
    do
	  echo ;
      echo "Downloading volume $DIR in ${ext[$i]} format..."
	  curl -o "$DIRBASE$DIR/$TITLEBASE$DIR${ext[$i]}" -L "$URLBASE$DIR/$DIR${ext[$i]}"
      i=$((i + 1))
    done
	echo "Finished volume $DIR..."
    INT=$((INT + 1))
	echo ;
	echo ;
  done
