#! /bin/bash

convert()
{
  ARTIST=$($(command -v metaflac) "$filename" --show-tag=ARTIST | sed s/.*=//g)
  TITLE=$($(command -v metaflac) "$filename" --show-tag=TITLE | sed s/.*=//g)
  ALBUM=$($(command -v metaflac) "$filename" --show-tag=ALBUM | sed s/.*=//g)
  GENRE=$($(command -v metaflac) "$filename" --show-tag=GENRE | sed s/.*=//g)
  TRACKNUMBER=$($(command -v metaflac) "$filename" --show-tag=TRACKNUMBER | sed s/.*=//g)
  DATE=$($(command -v metaflac) "$filename" --show-tag=DATE | sed s/.*=//g)

  $(command -v flac) -c -d "$filename" | $(command -v lame) -m j -q 0 --vbr-new -V 0 -s 44.1 - "$OUTF"

  $(command -v id3tag) -s "$TITLE" -t "${TRACKNUMBER:-0}" -a "$ARTIST" -A "$ALBUM" -y "$DATE" -g "${GENRE:-12}" "$OUTF"

  echo "$OUTF created."
}

find_art()
{
  IMAGEDIR=$($(command -v dirname) "$filename")
  COVER=$($(command -v find) "$IMAGEDIR" -type f -name *.jpg -o -name *.jpeg -o -name *.png)
  if test -f "$COVER"; then
    echo "Cover art found: $COVER"
    $(command -v eyeD3) --add-image="$COVER":FRONT_COVER "$OUTF"
  fi
}

remove() 
{
  $(command -v rm) -rf "$filename"
  echo "$filename removed."
}

simulate()
{
  echo "$filename created."
  echo "$OUTF removed."
}

wipe_art()
{
  $(command -v find) "$TARGET" -type f -name *.jpg -o -name *.jpeg -o -name *.png |
    while read imagename; do
      if test -f "$imagename"; then
        $(command -v rm) -rf "$imagename"
        echo "Cover art removed: $imagename"
      fi
    done
}

ARGS=":a:c::e:f:r:s:t:w:"

while getopts $ARGS option; do
  case "${option}"
    in
      a) ART=${OPTARG}
      ;;
      c) CONVERT=${OPTARG}
      ;;
      e) EXTENSION=${OPTARG}
      ;;
      f) FIND=${OPTARG}
      ;;
      r) REMOVE=${OPTARG}
      ;;
      s) SIMULATE=${OPTARG}
      ;;
      t) TARGET=${OPTARG}
      ;;
      w) WIPEART=${OPTARG}
      ;;
      \?) echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

$(command -v find) $TARGET -name "*.$EXTENSION" |
  while read filename; do
    if [[ "$EXTENSION" == "flac" ]]; then
      OUTF=${filename%.flac}.mp3
    else
      OUTF=$filename
    fi

    if [[ "$FIND" == "true" ]]; then
      echo "FOUND: $filename"
    fi

    if [[ "$SIMULATE" == "true" ]]; then
      simulate
    fi

    if [[ "$CONVERT" == "true" && "$SIMLUATE" != "true" ]]; then
      convert
    fi

    if [[ "$ART" == "true" ]]; then
      find_art
    fi

    if [[ "$REMOVE" == "true" && "$SIMLUATE" != "true" ]]; then
      remove   
    fi
  done;

  if [[ "$WIPEART" == "true" ]]; then
    wipe_art
  fi
