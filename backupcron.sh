#!/bin/sh
mysqluser=''
mysqlpassword=''
mysqlhost=''
mysqldb=''
backuppath=''
backupprefix=''

`which mysqldump` --opt --skip-add-locks --user=$mysqluser --password=$mysqlpassword --host=$mysqlhost $mysqldb | `which gzip` > $backuppath/$backupprefix`date "+%Y-%m-%d-%H-%M-%S"`.gz
`which find` $backuppath/*.gz -mtime +45 -delete
